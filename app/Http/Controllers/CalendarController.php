<?php

namespace App\Http\Controllers;

use App\Models\GoogleAccessToken;
use Google\Auth\HttpHandler\Guzzle7HttpHandler;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CalendarController extends Controller
{
    
    public function index(){
        $userId = 1;

        $token = GoogleAccessToken::where('user_id', $userId)->first(
            [
                'access_token',
                'scope',
                'refresh_token',
                'token_type',
                'expires_in',
                'created',
            ]
        );
        
        $config = config('calendar.google');  

        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes( Google_Service_Calendar::CALENDAR_EVENTS);
        $client->setAuthConfig( $config );                
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $client->setAccessToken( $token->toArray() );


        $service = new Google_Service_Calendar($client);

        // Print the next 10 events on the user's calendar.
        $calendarId = 'primary';
        $optParams = array(
            'maxResults' => 20,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => date('c'),
        );
        $results = $service->events->listEvents($calendarId, $optParams);
        $events = $results->getItems();

        if (empty($events)) {
            print "No upcoming events found.\n";
        } else {
            print "Upcoming events:\n";
            foreach ($events as $event) {
                var_dump($event);                                
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                printf("%s (%s)<br>", $event->getSummary(), $start);
            }
        }
    }

    public function create(){
        $userId = 1;

        $token = GoogleAccessToken::where('user_id', $userId)->first(
            [
                'access_token',
                'scope',
                'refresh_token',
                'token_type',
                'expires_in',
                'created',
            ]
        );
        
        $config = config('calendar.google');  

        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes( Google_Service_Calendar::CALENDAR_EVENTS);
        $client->setAuthConfig( $config );                
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $client->setAccessToken( $token->toArray() );


        $service = new Google_Service_Calendar($client);

        $event = new Google_Service_Calendar_Event(array(
            'summary' => 'Google I/O 2020',
            'location' => '800 Howard St., San Francisco, CA 94103',
            'description' => 'A chance to hear more about Google\'s developer products.',
            'start' => array(
              'dateTime' => '2020-12-31T09:00:00-07:00',
              'timeZone' => 'America/Mexico_City',
            ),
            'end' => array(
              'dateTime' => '2020-12-31T17:00:00-07:00',
              'timeZone' => 'America/Mexico_City',
            ),            
            'attendees' => array(
              #array('email' => 'gonzalo.adriancr@gmail.com'),              
            ),
            'reminders' => array(
              'useDefault' => FALSE,
              'overrides' => array(
                array('method' => 'email', 'minutes' => 24 * 60),
                array('method' => 'popup', 'minutes' => 10),
              ),
            ),
          ));
          
          $calendarId = 'primary';          
          $event = $service->events->insert($calendarId, $event);

    }

    public function update(){

        #5pc3ekkk8nl18gpp40ru4ol6uc

        $userId = 1;

        $token = GoogleAccessToken::where('user_id', $userId)->first(
            [
                'access_token',
                'scope',
                'refresh_token',
                'token_type',
                'expires_in',
                'created',
            ]
        );
        
        $config = config('calendar.google');  

        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes( Google_Service_Calendar::CALENDAR_EVENTS);
        $client->setAuthConfig( $config );                
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $client->setAccessToken( $token->toArray() );


        $service = new Google_Service_Calendar($client);

        $event = new Google_Service_Calendar_Event(array(
            'summary' => 'Google I/O 2020 1.3',
            'location' => '800 Howard St., San Francisco, CA 94103',
            'description' => 'A chance to hear more about Google\'s developer products.',
            'start' => array(
                'dateTime' => '2020-12-31T09:00:00-07:00',
                'timeZone' => 'America/Mexico_City',
              ),
            'end' => array(
                'dateTime' => '2020-12-31T17:00:00-07:00',
                'timeZone' => 'America/Mexico_City',
              ),  
              'reminders' => array(
                'useDefault' => FALSE,
                'overrides' => array(
                  array('method' => 'email', 'minutes' => 24 * 60),
                  array('method' => 'popup', 'minutes' => 10),
                ),
              ),
          ));
          
          $calendarId = 'primary';       
          
          $event = $service->events->update($calendarId, 'i11kk22dogkv0i1ss86p0utd20', $event);
          #$event = $service->events->insert($calendarId, $event);
    }

    public function delete(){

        $userId = 1;

        $token = GoogleAccessToken::where('user_id', $userId)->first(
            [
                'access_token',
                'scope',
                'refresh_token',
                'token_type',
                'expires_in',
                'created',
            ]
        );
        
        $config = config('calendar.google');  

        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes( Google_Service_Calendar::CALENDAR_EVENTS);
        $client->setAuthConfig( $config );                
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $client->setAccessToken( $token->toArray() );


        $service = new Google_Service_Calendar($client);
        $calendarId = 'primary';       

        $service->events->delete($calendarId, 'i11kk22dogkv0i1ss86p0utd20');

    }

    public function subscribe(){
        $userId = 1;

        $googleToken = GoogleAccessToken::where('user_id', $userId)->first();

        $token = $googleToken->access_token;
        $calendarId = 'primary';        
        $id = $userId . Str::random(10);

        $expiration = time() + 120 ;
        echo $token;
        $urlWH = 'https://capigooglecalendar.tk/webHook';

        $response = Http::withToken($token)
                    ->post('https://www.googleapis.com/calendar/v3/calendars/'. $calendarId . '/events/watch', [
                        "id" => $id,
                        "type" => 'web_hook',
                        "address" => $urlWH,
                        #"expiration" => $expiration,  
                        'params' => [
                            'ttl' => '3600'
                        ]
                    ]);

        $response;

        var_dump($response->body());

    }

    public function desuscribe(){
        $userId = 1;

        $googleToken = GoogleAccessToken::where('user_id', $userId)->first();

        $token = $googleToken->access_token;
        $calendarId = 'primary';        
        $id = $userId . Str::random(10);        

        $response = Http::withToken($token)
                    ->post('https://www.googleapis.com/calendar/v3/channels/stop', [
                        "id" => '1zG51k8wCkr',
                        "resourceId" => 'rfwtG1AMedI71thd0OW1pl51teg',                        
                    ]);

        $response;

        var_dump($response->body());
    }

    public function webHook(){

        Log::info(request()->all());

    }

}
