<?php

namespace App\Http\Controllers;

use App\Models\GoogleAccessToken;
use App\Models\GoogleFlagLogIn;
use Exception;
use Google_Client;
use Google_Service_Calendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    
    
    public function logIn(){
        Auth::loginUsingId(1);
        $user = Auth::user();              
        $random = Str::random(8);      
        $oAuthState = $user->id . '-' . time() . '-'. $random;
        
        $config = config('calendar.google');  

        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes( Google_Service_Calendar::CALENDAR_EVENTS);
        $client->setAuthConfig( $config );                
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        $client->setState($oAuthState);

        GoogleFlagLogIn::where('user_id', $user->id)->delete();
        GoogleFlagLogIn::create(['user_id' => $user->id, 'oauthState' => $oAuthState]);

        $authUrl = $client->createAuthUrl();
        
        return redirect()->away($authUrl);
    }

    public function logInCallback(){
        $req = request()->all();

        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes( Google_Service_Calendar::CALENDAR_EVENTS);
        $client->setAuthConfig( config('calendar.google') );
        $client->setAccessType('offline');
        $code = trim( $req['code'] );
        $accessToken = $client->fetchAccessTokenWithAuthCode( $code );
        
        $client->setAccessToken($accessToken);

        $final = array_merge( $req, $client->getAccessToken());
        
        $flag = GoogleFlagLogIn::where('oauthState', $req['state'])->first();

        if($flag){            
            GoogleAccessToken::create([
                'user_id'    => $flag->user_id,
                'access_token'  => $accessToken['access_token'],
                'refresh_token' => $accessToken['refresh_token'],
                'token_type' => $accessToken['token_type'],
                'expires_in' => $accessToken['expires_in'],
                'scope'      => $accessToken['scope'],
                'created'    => $accessToken['created'],
            ]);

            GoogleFlagLogIn::where('user_id', $flag->user_id)->delete();

            return 'Login Exitoso!';
        }

        throw new \Exception('No fue posible obtener Token');

    }


}
