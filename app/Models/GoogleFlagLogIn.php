<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoogleFlagLogIn extends Model
{
    use HasFactory;

    protected $table = 'google_flag_log_in';
    protected $primary = 'user_id';
    protected $fillable = ['oauthState', 'user_id'];
    public $timestamps = false;
}
