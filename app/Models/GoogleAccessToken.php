<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoogleAccessToken extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'access_token',
        'scope',
        'refresh_token',
        'token_type',
        'expires_in',
        'created',
    ];
}
