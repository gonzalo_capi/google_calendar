<?php

use Illuminate\Support\Str;

return [      

    'google' => [

        'web' => [
            'client_id'  => env('GOOGLE_CLIENT_ID'),
            'project_id' => env('GOOGLE_PROJECT_ID'),
            'auth_uri'   => env('GOOGLE_AUTH_URI'),
            'token_uri'  => env('GOOGLE_TOKEN_URI'),
            'auth_provider_x509_cert_url' => env('GOOGLE_AUTH_PROVIDER_X509_CERT_URL'),
            'client_secret' => env('GOOGLE_CLIENT_SECRET'),
            'redirect_uris' => [ env('GOOGLE_REDIRECT_LOGIN_URI') ],
        ],
        
    ],

  

];
