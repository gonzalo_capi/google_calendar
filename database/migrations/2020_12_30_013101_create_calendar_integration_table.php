<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarIntegrationTable extends Migration
{
  
    public function up()
    {
        Schema::create('calendar_integrations', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('provider');

            $table->timestamps();
        });

    }

   
    public function down()
    {
        Schema::dropIfExists('calendar_integrations');
    }
}