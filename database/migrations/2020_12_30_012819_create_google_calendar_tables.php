<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoogleCalendarTables extends Migration
{
  
    public function up()
    {
        Schema::create('google_access_tokens', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('user_id');            
            $table->text('access_token');            
            $table->text('refresh_token');          
            $table->text('token_type'); 
            $table->text('scope'); 

            $table->string('expires_in');  
            $table->string('created');                         
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('google_flag_log_in', function(Blueprint $table){
            $table->increments('user_id');

            $table->text('oauthState');
        });

    }

   
    public function down()
    {
        Schema::dropIfExists('google_access_token');
        Schema::dropIfExists('google_flag_log_in');
    }
}