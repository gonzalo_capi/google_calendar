<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CalendarController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('login', [ AuthController::class, 'logIn' ]);

Route::get('googleOAuth/loginCallback', [ AuthController::class, 'logInCallback' ]);

Route::get('calendar', [ CalendarController::class, 'index' ]);
Route::get('calendar/create', [ CalendarController::class, 'create' ]);
Route::get('calendar/update', [ CalendarController::class, 'update' ]);
Route::get('calendar/delete', [ CalendarController::class, 'delete' ]);

Route::get('subscribe',  [ CalendarController::class, 'subscribe' ] );
Route::get('desuscribe',  [ CalendarController::class, 'desuscribe' ] );



Route::get('webHook',  [ CalendarController::class, 'webHook' ] );

